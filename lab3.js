   
   //Primera clase
class CalculadorAritmetico {
   //Atributos
    x;
    y;

   //Metodos
    Sumar() {
        let r;
        r = this.x - (-this.y);
        return r;
        
    }
    Restar() {
        let r;
        r = this.x - this.y;
        return r;

    }
    Modular() {
        let r;
        r = this.x % this.y;
        return r;

    }

}

function operaciones(n) {
    let respuesta;
    let r;

    //Primer Objeto
    let nuevoCalculadorAritmetico = new CalculadorAritmetico;

    nuevoCalculadorAritmetico.x = document.getElementById("X").value;
    nuevoCalculadorAritmetico.y = document.getElementById("Y").value;

    if (n == 1) {
        r = nuevoCalculadorAritmetico.Sumar();
        respuesta = r;
    }
    if (n == 2) {
        r = nuevoCalculadorAritmetico.Restar();
        respuesta = r;
    }
    if (n == 3) {
        r = nuevoCalculadorAritmetico.Modular();
        respuesta = r;
    }
    document.getElementById("resultado").value = respuesta;
}

    //Segunda clase
class Calculador{
    //Atributos
    x;
    y;

    //Metodos
    Potenciar() {
        let r;
        r = Math.pow(this.x, this.y);
        return r;
    }
    Logaritmo_x() {
        let r;
        r = Math.log(this.x);
        return r;
    }
    Logaritmo_y() {
        let r;
        r = Math.log(this.y);
        return r;
    }

}

function operaciones_2(n) {
    let respuesta;
    let r;

    //Segundo Objeto
    let nuevoCalculador= new Calculador;

    nuevoCalculador.x = document.getElementById("X").value;
    nuevoCalculador.y = document.getElementById("Y").value;

    if (n == 1) {
        r = Number(nuevoCalculador.Potenciar());
        respuesta = r;
    }
    if (n == 2) {
        r = Number(nuevoCalculador.Logaritmo_x());
        respuesta = r;
    }
    if (n == 3) {
        r = Number(nuevoCalculador.Logaritmo_y());
        respuesta = r;
    }
    document.getElementById("resultado").value = respuesta;
}
